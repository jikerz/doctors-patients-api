'use strict';
var faker = require('faker')

module.exports = function(app) {
  /*
   * The `app` object provides access to a variety of LoopBack resources such as
   * models (e.g. `app.models.YourModelName`) or data sources (e.g.
   * `app.datasources.YourDataSource`). See
   * http://docs.strongloop.com/display/public/LB/Working+with+LoopBack+objects
   * for more info.
   */

  function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  var generated = {
    patients: [],
    doctors: [],
    appointments: []
  }

  app.datasources.db.automigrate('Patient', function (err) {
    if (err) return cb(err)
    var Patient = app.models.Patient

    for (var i = 0; i < 25; i++) {
      let pObj = {
        firstname: faker.name.firstName(),
        lastname: faker.name.lastName(),
        age: getRandomIntInclusive(1, 105),
        email: faker.internet.email(),
        username: faker.internet.userName(),
        password: 'drone',
        mailingAddress: faker.address.streetAddress(),
        mailingCity: faker.address.city(),
        mailingState: faker.address.state(),
        mailingZip: faker.address.zipCode(),
        phone: faker.phone.phoneNumber()
      }
      generated.patients.push(pObj)
    }

    let known = {
      firstname: 'John',
      lastname: 'Smith',
      age: 30,
      email: 'jsmith@gmail.com',
      username: 'jsmith',
      password: 'drone',
      mailingAddress: faker.address.streetAddress(),
      mailingCity: faker.address.city(),
      mailingState: faker.address.state(),
      mailingZip: faker.address.zipCode(),
      phone: faker.phone.phoneNumber()
    }
    generated.patients.push(known)

    Patient.create(generated.patients, function(err, patients) {
      if (err) {
        throw err
      }
      console.log('Patients created');
    })
  })

  app.datasources.db.automigrate('Doctor', function (err) {
    if (err) return cb(err);

    var Doctor = app.models.Doctor;
    for(var i = 0; i < 5; i++) {
      let tmp = {
        firstname: faker.name.firstName(),
        lastname: faker.name.lastName(),
        email: faker.internet.email(),
        username: faker.internet.userName(),
        password: 'healme'
      }
      generated.doctors.push(tmp)
    }

    let known = {
      firstname: 'Chris',
      lastname: 'Turk',
      email: 'cturk@gmail.com',
      username: 'cturk',
      password: 'healme'
    }
    generated.doctors.push(known)

    Doctor.create(generated.doctors, function (err, doctors) {
        if (err) {
          throw err
        }
        console.log('Doctors created');
    })
  })

  app.datasources.db.automigrate('Appointment', function (err) {
    if (err) return cb(err);
    var Appointment = app.models.Appointment;

    for(var i = 0; i < 30; i++) {
      let tmp = {
        date: faker.date.between('2017-04-01', '2017-07-01'),
        subject: faker.random.words(),
        status: 'Pending Doctor',
        patientId: getRandomIntInclusive(1, generated.patients.length),
        doctorId: getRandomIntInclusive(1, generated.doctors.length)
      }
      generated.appointments.push(tmp)
    }

    Appointment.create(generated.appointments, function (err, appointments) {
      if (err) throw err;
      console.log('Appointments created: \n', appointments)
    })
  })
};
